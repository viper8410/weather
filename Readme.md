# First step is to have installed Python3 on your computer

# To see which version of Python 3 you have installed, open a command prompt and run

$ python3 --version

# If you are using Ubuntu 16.10 or newer, then you can easily install Python 3.6 with the following commands:

$ sudo apt-get update
$ sudo apt-get install python3.6

# If you’re using another version of Ubuntu (e.g. the latest LTS release) or you want to use a more current Python, we recommend using the deadsnakes PPA to install Python 3.8:

$ sudo apt-get install software-properties-common
$ sudo add-apt-repository ppa:deadsnakes/ppa
$ sudo apt-get update
$ sudo apt-get install python3.8

# If you are using other Linux distribution, chances are you already have Python 3 pre-installed as well. If not, use your distribution’s package manager. For example on Fedora, you would use dnf:

$ sudo dnf install python3

# After you have installed Python3 you must install BeautifulSoup

#To install bs4 on Debian or Ubuntu linux using system package manager, run the below command −

$sudo apt-get install python-bs4 (for python 2.x)
$sudo apt-get install python3-bs4 (for python 3.x)

#You can install bs4 using easy_install or pip (in case you find problem in installing using system packager).

$easy_install beautifulsoup4
$pip install beautifulsoup4

# For Windows Machine run that command

>pip install beautifulsoup4

# After that you just run the main.py file

python3 main.py

# Now enter the desired city and the script will tell you about the weather condition in that city 
